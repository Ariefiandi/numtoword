#ifndef MYNUMBER_H
#define MYNUMBER_H

#include <string>
#include<vector>
#include "MyLanguage.h"

class MyNumber
{
	
public:
	
	MyNumber(std::string Number);
	MyNumber();
	~MyNumber();

	std::string getWords();
	void converterNumberToWord();
	void setWords(std::string words);
	int getNumbers();
	void setNumbers(std::string num);

private :
	std::string Number;
	std::string Words;
	MyLanguage BaseLanguage;
	
	bool isNegative;
	bool isValid;
	
	std::string BizOpConvertNumberToWord();
	bool ValidateInputNumber();
	bool NegativeChceker();
	bool Validator();
		
};

#endif MYNUMBER_H

