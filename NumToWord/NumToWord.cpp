#include <iostream>
#include <string>
#include "MyNumber.h"

int main()
{
	bool isNegative = false;
	char repeat = 'n';
	std::string  inputNum; // for user entered number
	
	do{
		std::cout << "Number = "; std::cin >> inputNum;
	
		MyNumber NumToWord(inputNum);
		NumToWord.converterNumberToWord();//should be like controller in bigger scope
		std::cout << NumToWord.getWords() << std::endl;
		//repeat the process
		std::cout << std::endl << "Repeat? (y/n): ";
		std::cin >> repeat;
	}
	
	while (repeat == 'y');
	
	std::cout << std::endl;
	return 0;
}