#ifndef MYLANGUAGE_H
#define MYLANGUAGE_H

#include<string>
#include<vector>

class MyLanguage{

public:
	//dictionary for selected language
	std::vector<std::string> onesName;
	std::vector<std::string> teensName;
	std::vector<std::string> tensName;
	std::vector<std::string> illion_preName;
	std::vector<std::string> decillion_preName;
	std::vector<std::string> helperName;
	
	MyLanguage();
	MyLanguage(std::string language);
	void setBaseLanguage(std::string language);
	~MyLanguage();

private:
	std::string Lang;
	
	template<typename Out>
	void split(const std::string &s, char del, Out res);
	void NumberMapper(std::string source,std::vector<std::string> & numVector);
	void getBaseLanguage();

};

#endif MYLANGUAGE_H 