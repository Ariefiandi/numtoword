#include <iostream>
#include "MyNumber.h"
#include "MyLanguage.h"
#include <string>
#include <vector>
#include <sstream>


MyNumber::MyNumber(std::string Num) :isNegative(false), isValid(true), BaseLanguage("EN_US.txt")
{
	setNumbers(Num);
	setWords("");
	
}

MyNumber::~MyNumber()
{
}

void MyNumber::converterNumberToWord(){
	setWords(BizOpConvertNumberToWord());
}

std::string MyNumber::BizOpConvertNumberToWord(){
	isNegative = NegativeChceker();
	isValid = Validator();
	std::string _words = "";
	if (isValid){
		
		while (Number.size() % 3 != 0)
			Number = '0' + Number;
		
		if (isNegative)
			_words.append(BaseLanguage.helperName[0] + " ");
		
		for (unsigned int i = 0; i < Number.size(); i += 3){// skip if all 3 digits == '0'
			if (Number[i] == '0' && Number[i + 1] == '0' && Number[i + 2] == '0')
				continue;

			if (Number[i + 0] > '0')//hundreds place
				_words.append(BaseLanguage.onesName[Number[i + 0] - '0' - 1] + " " + BaseLanguage.helperName[1] + " ");
		

			if (Number[i + 1] == '0' || Number[i + 1] > '1')//tens and ones digits for non-teens case
			{
				if (Number[i + 1] > '1')
					_words.append(BaseLanguage.tensName[Number[i + 1] - '0' - 2] + " ");
	
				if (Number[i + 2] > '0'){
					if ((Number.size() - i) == 3){ // check if its last digit and it ones 
						_words.pop_back();
						_words.append("-");
					}
					_words.append(BaseLanguage.onesName[Number[i + 2] - '0' - 1] + " ");
				}
	
			}
			else// special teens case
				_words.append(BaseLanguage.teensName[Number[i + 2] - '0'] + " ");	
			// naming each factor of 1,000
			unsigned int j = (Number.size() - i) / 3;
			if (j == 2)
				_words.append(BaseLanguage.helperName[2] + " ");
		
			else if (j > 2)
			{

				if (j <= 12)
					_words.append(BaseLanguage.illion_preName[j - 3]);// 'xx' before "illion" cases
				else if (j <= 21)
					_words.append(BaseLanguage.decillion_preName[j - 13] + "dec");// 'xx' before "dec" + "illion" cases
				else if (j == 22)
					_words.append(BaseLanguage.helperName[3] + " ");// special 'xx' before "vigint" + "illion" case

				_words.append(BaseLanguage.helperName[4] + " ");// "illion" suffix
			}
		}
	}
	return _words;

}

bool MyNumber::NegativeChceker(){	
	if (Number[0] == '-'){
			Number.erase(0, 1);
		return true;
	}
	else 
		return false;
	
}

bool MyNumber::Validator(){
	// validator to check that all characters are digits
	for (unsigned int i = 0; i < Number.size(); ++i)
	if (Number[i] < '0' || Number[i] > '9')
	{
		std::cout << "Your entry contains invalid characters." << std::endl;
		return false;
	}
	//validator to check that number of digits is not too high.
	if (Number.size() > 66)
	{
		std::cout << "The number's too big! Please try again with smaller number." << std::endl;
		return false;
	}

	return true;
}


void MyNumber::setWords(std::string words){
	Words = words;
}

std::string MyNumber::getWords(){
	return Words;
}

void MyNumber::setNumbers(std::string num){
	Number = num;
}


