#include "MyLanguage.h"
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <fstream>

MyLanguage::MyLanguage(){
}

MyLanguage::MyLanguage(std::string language){
	setBaseLanguage(language);
	getBaseLanguage();
}
MyLanguage::~MyLanguage(){
}

void MyLanguage::setBaseLanguage(std::string language){
	Lang = language;
}

void MyLanguage::getBaseLanguage(){
	std::ifstream file(Lang);
	std::string line;
	
	for (int i = 0; std::getline(file, line); ++i){
		if (i == 0)
			NumberMapper(line, onesName);
		else if (i == 1)
			NumberMapper(line, teensName);
		else if (i == 2)
			NumberMapper(line, tensName);
		else if (i == 3)
			NumberMapper(line, illion_preName);
		else if (i == 4)
			NumberMapper(line, decillion_preName);
		else if (i == 5)
			NumberMapper(line, helperName);
		else
			break;
	}
}



void MyLanguage::NumberMapper(std::string source, std::vector<std::string> & numVector){
	split(source, ',', std::back_inserter(numVector));
}

template<typename Out>
void MyLanguage::split(const std::string &s, char delim, Out result) {
	std::stringstream ss;
	ss.str(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		*(result++) = item;
	}
}